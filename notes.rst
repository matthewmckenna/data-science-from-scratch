##################################
data science from scratch - notes
##################################
:date: 2018-02-02 17:03 GMT
:author: Matthew McKenna


************
probability
************

disjoint or mutually exclusive events
======================================
if A_1 and A_2 represent two disjoint outcomes then the probability that one
of them occurs is::

 P(A_1 or A_2) = P(A_1) + P(A_2)

if there are many disjoint outcomes, this generalises to
::

 P(A_1) + P(A_2) + ... + P(A_k)

general addition rule
======================
if ``A`` and ``B`` are two events, disjoint or not, the probability that at least one of
them will occur is::

 P(A or B) = P(A) + P(B) - P(A and B)

where ``P(A and B)`` is the probability that both events occur.

independence
=============

multiplication rule for independent processes
----------------------------------------------
if ``A`` and ``B`` represent events from two different and independent processes,
then the probability that both ``A`` and ``B`` occur can be calculated as the
product of their separate probabilities::

 P(A and B) = P(A) * P(B)

if there are ``k`` events from ``k`` independent processes this generalises to::

 P(A_1) * P(A_2) * ... * P(A_k)

another way of writing the above
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
for independent events, the probability of ``E AND F`` is equal to the
probability of ``E`` times the probability of ``F``::

 P(E and F) = P(E) * P(F)

conditional probability
========================
the conditional probability of the outcome of interest ``A`` given condition
``B`` is computed as::

 P(A|B) = P(A and B) / P(B)

general multiplication rule
----------------------------
if ``A`` and ``B`` represent two outcomes or events, then::

 P(A and B) = P(A|B) * P(B)

it can be useful to think of ``A`` as the outcome of interest and ``B`` as the
condition.

another way of writing the above rules
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
for events that are not necessarily independent, we define the probability of
"E given F", or "E conditional on F" as::

 P(E|F) = P(E and F) / P(F)

which can also be written as::

 P(E and F) = P(E|F) * P(F)

when ``E`` and ``F`` are independent the above gives::

 P(E|F) = P(E)

**workings**
::

 P(E|F) = P(E and F) / P(F)
 P(E|F) = P(E) * P(F) / P(F)
 P(E|F) = P(E)

conditional probability example
================================
consider a family with two unknown children

we make the following assumptions:

1. each child is equally likely to be a boy or a girl
2. the gender of the second child is independent of the gender of the first
   child

so, the event "no girls" has probability 1/4, the event "one girl, one boy" has
the probability 1/2, and the event "two girls" has the probability 1/4.

we now ask what is the probability of the event "both children are girls"
(``B``) conditional on the event "the older child is a girl" (``G``)
::

 P(B|G) = P(B and G) / P(G) = P(B) / P(G) = 1/4 / 1/2 = 1/2

above, ``P(B and G)`` becomes ``P(B)`` because ``P(B and G)`` expands to::

 P(B and G) = P(child1=girl and child2=girl and child1=girl)
 P(B and G) = P(child1=girl and child2=girl)
 P(B and G) = P(B)

now we ask what is the probability that "both children are girls" conditional
on the event that "at least one child is a girl" (``L``).
::

 P(B|L) = P(B and L) / P(L) = P(B) / P(L)

we see that ``P(B and L)`` expands to::

 P(B and L) = P((child1=girl and child2=girl) and (child1=girl or child2=girl))
 P(B and L) = P(child1=girl and child2=girl and child1=girl)
 or
 P(B and L) = P(child1=girl and child2=girl and child2=girl)

either way, this reduces to::

 P(B and L) = P(child1=girl and child2=girl)
 P(B and L) = P(B)

now, for ``P(L)``, we can say this is equivalent to ``1 - P("no girls")``::

 P(L) = 1 - P(no girls)
 P(L) = 1 - 1/4
 P(L) = 3/4

so, our equation becomes::

 P(B|L) = P(B and L) / P(L) = P(B) / P(L) = 1/4 / 3/4 = 1/3

bayes theorem
==============
from the definition of conditional probability we have::

 P(E|F) = P(E and F) / P(F)
 P(E|F) = P(F|E)P(E) / P(F)

we can split the event ``F`` into the two mutually exclusive events ``F and E``
and ``F and not E`` (or ``F and ¬E``).
::

 P(F) = P(F and E) + P(F and ¬E)

so we have::

 P(E|F) = P(E and F) / P(F)
 P(E|F) = P(F|E)P(E) / P(F)
 P(E|F) = P(F|E)P(E) / [P(F and E) + P(F and ¬E)]
 P(E|F) = P(F|E)P(E) / [P(F|E)P(E) + P(F|¬E)P(¬E)]

imagine a certain disease affects 1 in every 10,000 people. also imagine that
there is a test for the disease that gives the correct result 99% of the time.

let us use ``T`` for the event "the test is positive" and ``D`` for the event
"you have the disease".

bayes theorem says that the probability that you have the disease conditional on
testing positive is::

 P(D|T) = P(D and T) / P(T)
 P(D|T) = P(T|D)P(D) / P(T)
 P(D|T) = P(T|D)P(D) / [P(T|D)P(D) + P(T|¬D)P(¬D)]

* we know that ``P(T|D)`` (the probability of testing positive given you have the
  disease) is 0.99.
* we know that ``P(D)`` (any given person has the disease) is 1/10,000 = 0.0001.
* we know that ``P(T|¬D)`` (probability of testing positive given you do not have
  the disease) is 0.01.
* we know that ``P(¬D)`` (the probability that any given person does not have
  the disease) is 0.9999.

subbing the above into the equation we have::

 P(D|T) = P(T|D)P(D) / [P(T|D)P(D) + P(T|¬D)P(¬D)]
 P(D|T) = 0.99 * 0.0001 / [0.99 * 0.0001 + 0.01 * 0.9999]
 P(D|T) = 0.0098

that is, less than 1% of the people who test positive actually have the disease.

most doctors will guess that ``P(D|T)`` is approximately 2.

a more intuitive way to see this is to imagine a population of 1,000,000 people.
you'd expect 100 of them to have the disease, and 99 of the 100 to test positive.
on the other hand, you'd expect 999,900 to not have the disease and 9999 to test
positive. this means that you'd expect only 99 out of (99+9999) positive testers
to actually have the disease.

random variables
================
a random variable is a variable whose possible values have an associated
probability distribution.

sometimes we talk about the *expected value* of a random variable which is the
average of its values weighted by their probabilities.


continuous distributions
=========================
a coin flip corresponds to a discrete distribution, though often we'll want to
model distributions with continuous outcomes. for example, a uniform
distribution puts equal weight on all the numbers between 0 and 1.

since there are infinitely many numbers between 0 and 1, this means that the
weight it assigns to individual points must necessarily be zero. for this
reason, we represent a continuous distribution with a  *probability density
function* (pdf) such that the probability of seeing a value in a certain interval
equals the integral of the density function over the interval.

if a distribution has density function *f*, then the probability of seeing a
value between *x* and *x+h* is approximately *h \* f(x)* if *h* is small

Python's ``random.random()`` is a pseudorandom variable with a uniform density.

often more interested in the *cumulative density function* (cdf), which gives
the probability that a random variable is less than or equal to a certain value.


the normal distribution
========================
described by two parameters: its mean, mu and its standard deviation, sigma.

the mean indicates where the distribution is centred, and the standard deviation
is how "wide" it is.

it is described by the function:

.. math::

 f(x|\mu, \sigma) = \frac{1}{\sqrt{2 \pi \sigma^2}} exp \left(- \frac{(x - \mu)^2}{2 \sigma^2}\right)

when :math:`\mu = 0` and :math:`\sigma = 1`, it's called the *standard normal
distribution*. If *Z* is a standard normal random variable, then it turns out
that:

.. math::

 X = \sigma Z + \mu

is also normal, but with mean :math:`\mu` and standard deviation :math:`\sigma`.
conversely, if *X* is a normal random variable with mean :math:`\mu` and
standard deviation :math:`\sigma`,

.. math::

 Z = \frac{(X - \mu)}{\sigma}

is a standard normal variable.


the central limit theorem
==========================
one of the reasons the normal distribution is so useful is the *central limit
theorem*. this says (in essence) that a random variable defined as the average
of a large number of independent and identically distributed random variables
is itself approximately normally distributed.

if x_1, ..., x_n are random variables with mean :math:`\mu` and standard
deviation :math:`\sigma`, and if *n* is large, then:

.. math::

 \frac{1}{n} (x_1 + ... + x_n)

is approximately normally distributed with mean :math:`\mu` and standard
deviation :math:`\frac{\sigma}{\sqrt{n}}`. in a more useful form:

.. math::

 \frac{(x_1 + ... + x_n) - \mu n}{\sigma \sqrt{n}}

is approximately normally distributed with mean 0 and standard deviation 1.

we can look at *binomial* random variables which have two parameters *n* and *p*
to illustrate this.

a binomial(n, p) random variable is simply the sum of *n* independent
Bernoulli(p) random variables, each of which equals 1 with probability
*p* and 0 with probability 1 - *p*.

the mean of a Bernoulli(p) variable is *p*, and its standard deviation is
:math:`\sqrt{p(1-p)}`. the central limit theorem says that as *n* gets large,
a binomial(n, p) variable is approximately a normal random variable with mean
:math:`\mu = np` and standard deviation :math:`\sigma = \sqrt{np(1-p)}`.

.. math::

 \mu = E[X] = P(X=0)*0 + P(X=1)*1

 \mu = E[X] = (1-p)*0 + (p)*1

 \mu = E[X] = p

the variance and standard deviation can be calculated as follows:

.. math::

 \sigma^2 = P(X=0)*(0-p)^2 + P(X=1)*(1-p)^2

 \sigma^2 = (1-p)*p^2 + p*(1-p)^2

 \sigma^2 = p * ((1-p)*p + (1-p)^2)

 \sigma^2 = p * (p - p^2 + 1 -2p + p^2)

 \sigma^2 = p(1 -p)

 \sigma = \sqrt{p(1 -p)}


*************************
hypothesis and inference
*************************

statistical hypothesis testing
===============================
imagine we have a coin and we want to test whether it is fair. we make the
assumption that the coin has some probability *p* of landing heads, and so our
null hypothesis is that the coin is fair -- i.e., p = 0.5. we will test this
against the alternative hypothesis p != 0.5.

our experiment will involve flipping the coin *n* times and counting the number
of heads *X*. each coin flip is a Bernoulli trial, which means *X* is a
Binomial(n, p) random variable, which we can approximate using the normal
distribution.

we flip the coin 1000 times. if our hypothesis of fairness is true, *X* should
be distributed approximately normally with mean 500 and standard deviation 15.8.

we need to make a decision about *significance* -- how willing our we to make a
*type I error* (false positive), in which we reject H_0 even though it's true.

this is usually set at 5% or 1%.
::

 normal_two_sided_bounds (469.01026640487555, 530.9897335951244)

assuming *p* really equals 0.5 (i.e., H_0 is true) there is just a 5% chance we
observe an *X* that lies outside this interval.

we're also often interested in the *power* of a test, which is the probability
of not making a *type II error*, in which we fail to reject H_0 even though it's
false. we must specify what exactly H_0 being false *means*.

let's check what happens if *p* is really 0.55, so that the coin is slightly
biased towards heads. we get the following running our experiment::

 type_2_probability 0.11345199870463285
 power 0.8865480012953671

imagine instead that our null hypothesis was that the coin is not biased towards
heads, or that p <= 0.5. in that case we want a *one-sided test* that rejects
the null hypothesis when *X* is much larger than 50, but not when *X* is smaller
than 50.

a 5% significance test involves using ``normal_probability_below`` to find the
cutoff below which 95% of the probability lies::

 hi = normal_upper_bound(0.95, mu_0, sigma_0)
 print(f'hi {hi}')  # 526 (< 531 since we need more probability in the upper tail)

 type_2_probability = normal_probability_below(hi, mu_1, sigma_1)
 power = 1 - type_2_probability  # = 0.936

this is a more powerful test, since it no longer rejects H_0 when *X* is below
469 (which is very unlikely to happen if H_1 is true) and instead rejects H_0
when *X* is between 526 and 531 (which is somewhat likely to happen if H_1 is
true).

we can also think about the previous experiment using *p-values*. instead of
choosing bounds based on some probability cutoff, we compute the probability
-- assuming H_0 is true -- that we would see a value at least as extreme as the
one we actually observed.

if we were to see 530 heads, we would compute::

 two_sided_p_value(529.5, mu_0, sigma_0)  # 0.062

we use 529.5 instead of 530 above. this is known as a *continuity correction*.
this is because ``normal_probability_between(529.5, 530.5, mu_0, sigma_0)`` is
a better estimate of the probability of seeing 530 heads than
``normal_probability_between(530, 531, mu_0, sigma_0)`` is.


confidence intervals
=====================
we can also construct a *confidence interval* around the observed value of the
parameter.

we can estimate the probability of the unfair coin by looking at the average
value of the Bernoulli variables corresponding to each flip -- 1 if heads, 0 if
tails.

if we observe 525 heads out of 1000 flips, we estimate *p* equals 0.525.

if we knew the exact value of *p*, the CLT tells us that the average of the
Bernoulli variables should be approximately normal, with mean *p* and standard
deviation::

 math.sqrt(p * (1 - p) / 1000)

here we don't know *p*, so instead we use our estimate::

 p_hat = 525 / 1000
 mu = p_hat
 sigma = math.sqrt(p_hat * (1 - p_hat) / 1000)  #0.0158
 normal_two_sided_bounds(0.95, mu, sigma)  # [0.4940, 0.5560]

if we repeat the experiment many times, 95% of the time the "true" parameter
will lie within the observed confidence interval. if instead we saw 540 heads::

 p_hat = 540 / 1000
 mu = p_hat
 sigma = math.sqrt(p_hat * (1 - p_hat) / 1000)  #0.0158
 normal_two_sided_bounds(0.95, mu, sigma)  # [0.5091, 0.5709]

here a "fair" coin doesn't lie in the confidence interval.


p-hacking
==========
a procedure that erroneously rejects the null hypothesis only 5% of the time
will -- by definition -- 5% of the time erroneously reject the null hypothesis.

if you're setting out to find "significant" results, you usually can. by testing
enough hypotheses against your data set one of them will almost certainly appear
significant. remove the right outliers and you can probably get your p-value
below 0.05.

this is sometimes called **p-hacking** and is in some ways a consequence of the
"inference from p-values framework".

to do good *science* you should determine your hypotheses before looking at the
data, clean your data without the hypotheses in mind, and you should keep in
mind that p-values are not substitutes for common sense.

example: running an a/b test
-----------------------------
need to choose between showing ad A or ad B to visitors on a site.

say ``N_A`` people see ad A and ``n_A`` click on it. each ad view can be thought
of as a Bernoulli trial, where ``p_A`` is the probability that someone clicks ad
A.

then (if ``N_A`` is large, which it is here) we know that ``n_A / N_A`` is
approximately a normal random variable with mean ``p_A`` and standard deviation
:math:`\sigma_A = \sqrt{p_A(1 - p_A)/N_A}`

similarly ``n_B/N_B`` is approximately a normal random variable with mean
``p_B`` and standard deviation :math:`\sigma_B = \sqrt{p_B(1 - p_B)/N_B}`.

if we assume those two normals are independent (seems reasonable, since the
individual Bernoulli trials ought to be), then their difference should also be
normal with mean ``p_B - p_A`` and standard deviation
:math:`\sqrt{\sigma_A^2 + \sigma_B^2}`.

NOTE: this is sort of cheating. math only works if we know the standard
deviations. here we're estimating from the data, which means we should be using
a t-distribution. however, for large enough data sets it's close enough that it
doesn't really make much difference.

this means we can test the null hypothesis that ``p_A`` and ``p_B`` are the
same, i.e., ``p_B - p_A = 0``


bayesian inference
===================
an alternative approach to inference involves treating the unknown parameters
themselves as random variables. we start with a *prior distribution* for the
parameters and the use the observed data and Bayes' Theorem to get an updated
*posterior distribution* for the parameters.

when the unknown parameter is a probability (coin-flip example) we often use a
prior from the *Beta distribution*.

generally speaking, the distribution centres its weight at::

 alpha / (alpha + beta)

and the larger alpha and beta are, the "tighter" the distribution is.

if alpha and beta are both ``1``, it's just the uniform distribution (centred
at 0.5, very dispersed). if alpha >> beta, most of the weight is near 1. if
alpha << beta, most of the weight is near 0.

let's say we assume a prior distribution on *p*. maybe we don't want to take a
stand on whether the coin is fair, and we choose alpha and beta to both equal 1.

or maybe we have a strong belief that it lands heads 55% of the time and we
choose alpha equals 55 and beta equals 45.

we flip the coin many times and observe ``h`` heads and ``t`` tails. Bayes'
Theorem tells us that the posterior distribution for *p* is again a Beta
distribution but with parameters ``alpha + h`` and ``beta + t``.

let's say we flip the coin 10 times and only see 3 heads.

if we started with the uniform prior, (Beta(1, 1)) the posterior distribution
would be Beta(4, 8) centred around 0.33.

if we started with Beta(20, 20) (that the coin was roughly fair) the posterior
would be a Beta(23, 27), centred around 0.46, indicating a revised belief that
maybe the coin is slightly biased towards tails.

if we start with a Beta(30, 10) (expressing belief that the coin was biased to
flip 75% heads), the posterior distribution would be a Beta(33, 17) centred
around 0.66. in that case, we still believe in a heads bias, but less strongly
than we did initially.

if we flipped the coin more times, the prior would matter less and less, until
we had nearly the same posterior distribution no matter what we started with.


*****************
gradient descent
*****************
a gradient is a vector of partial derivatives and gives the input direction in
which the function most quickly increases.

if you wish to minimise a function, you can pick a random starting point,
compute the gradient, take a small step in the opposite direction and repeat
with the new starting point.
