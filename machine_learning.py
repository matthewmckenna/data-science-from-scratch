#!/usr/bin/env python3
"""
Data Science from Scratch
Chapter 11: Machine Learning
"""
import random


def split_data(data, prob):
    """split data into fractions [prob, 1-prob]"""
    results = [], []
    # iterate through each `row` of `data` and append `row`
    # to either `results[0]` or `results[1]` depending on
    # the output of `random.random()`
    for row in data:
        results[0 if random.random() < prob else 1].append(row)
    return results


def train_test_split(X, y, test_pct):
    # data is a list of tuples of (observation, label)
    data = list(zip(X, y))
    # train and test are list of tuples of (observation, label)
    train, test = split_data(data, 1-test_pct)
    # X_train, X_test is a tuple of observations
    # y_train, y_test are tuples of class labels
    # list(zip(*train)):
    # [([1, 2, 3, 4], 0)] --> [([1, 2, 3, 4]), (0,)]
    X_train, y_train = list(zip(*train))
    X_test, y_test = list(zip(*test))

    return X_train, X_test, y_train, y_test


def accuracy(tp, fp, fn, tn):
    correct = tp + tn
    total = tp + fp + fn + tn
    return correct / total


def precision(tp, fp, fn, tn):
    """precision measures how accurate the positive predictions were"""
    return tp / (tp + fp)


def recall(tp, fp, fn, tn):
    """recall measures what fraction of the positives our model identified"""
    return tp / (tp + fn)


def f1_score(tp, fp, fn, tn):
    p = precision(tp, fp, fn, tn)
    r = recall(tp, fp, fn, tn)
    return (2*p*r) / (p+r)


if __name__ == '__main__':
    random.seed(0)
    X = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15, 16],
    ]
    y = [0, 0, 1, 0]

    split_data(range(100), 0.75)

    # often we'll have a matrix `X` of input variables and
    # a vector `y` of output variables.
    # we need to make sure to put corresponding values together
    # in either the training or test datasets.
    train_test_split(X, y, 0.25)

    # model = SomeKindOfModel()
    # X_train, X_test, y_train, y_test = train_test_split(X, y, 0.33)
    # model.train(X_train, y_train)
    # performance = model.test(X_test, y_test)

    # if the model performs well on the test data you can be more
    # confident that it's *fitting* rather than *overfitting*.

    # this can still go wrong.
    # imagine a dataset that consits of user activity -- one row
    # per user per week.
    # most users will appear in the training and test data, and
    # certain models may learn to *identify* users rather than
    # discover relationships.

    # a bigger problem is if you use the test/train split not
    # just to judge a model, but also *choose* from among many
    # models.
    # although each individual model may not be overfit, choosing
    # a model that performs best on the test set is a meta-training
    # that makes the test set function as a second training set.
    # in this situation you should also have a *validation* set
    # for choosing among trained models.

    # Correctness
    # Don't usually use accuracy to measure how good a model is.
    # Imagine building a binary classifier, each data point lies
    # in one of four categories:

    # True positive: "the message is spam, we predicted spam"
    # False positive (type I error): "the message is not spam, we predicted spam"
    # False negative (type II error): "the message is spam, we predicted not spam"
    # True negative: "the message is not spam, we predicted not spam"

    # these are often represented as a confusion matrix

    # -                  | spam           | not spam
    # ------------------ |--------------- | --------
    # predict "spam"     | True Positive  | False Positive
    # predict "not spam" | False Negative | True Negative

    # approximately 5 babies in 1000 are named Luke.
    # lifetime prevalence of leukemia is 1.4%, or 14/1000 people.
    # if we believe these are independent factors, and apply the
    # "Luke is for leukemia" test to 1 million people, we'd expect
    # a confusion matrix as follows:

    # -          | leukemia | no leukemia | total
    # ---------- |--------- |------------ | --------
    # "Luke"     | 70       | 4,930       | 5,000
    # not "Luke" | 13,930   | 981,070     | 995,000
    # total      | 14,000   | 986,000     | 1,000,000

    # we can then compute various statistics about model performance.

    # tp = 70
    # fp = 4930
    # fn = 13930
    # tn = 981070
    # accuracy = 0.98114 (~98%)
    print(f'accuracy(70, 4930, 13930, 981070): {accuracy(70, 4930, 13930, 981070)}')

    # look instead to use precision and recall.
    # precision measures how accurate our positive predictions were.
    # tp = 70
    # fp = 4930
    # fn = 13930
    # tn = 981070
    # precision = 0.014 (~1%)
    print(f'precision(70, 4930, 13930, 981070): {precision(70, 4930, 13930, 981070)}')

    # recall measures what fraction of the positives our model identified
    # tp = 70
    # fp = 4930
    # fn = 13930
    # tn = 981070
    # recall = 0.005 (0.5%)
    print(f'recall(70, 4930, 13930, 981070): {recall(70, 4930, 13930, 981070)}')

    # the precison and recall are terrible indicating a terrible model.
    # sometimes precision and recall are combined into the F1 score
    # this is the harmonic mean of precision and recall, and necessarily
    # lies between them
    print(f'f1_score(70, 4930, 13930, 981070): {f1_score(70, 4930, 13930, 981070)}')

    # a model that predicts "yes" when it's even a little bit confident
    # will probably have a high recall, but low precision.
    # a model that predicts "yes" only when it's extremely confident is
    # likely to have a low recall, and high precision.

    # alternatively, you can think of this as a trade-off between false
    # positives and false negatives.
    # saying "yes" too often will give you a lot of false positives.
    # saying "no" too often will give you lots of false negatives.

    # The Bias-Variance Trade-off
    # a degree 0 model (horizontal line) will make a lot of mistakes
    # for any training set (drawn from the same population).
    # this is representative of **high bias**.
    # however, any two randomly chosen training sets should give fairly
    # similar models (since two randomly chosen training sets should
    # have fairly similar averages).
    # so we say that it has **low variance**.
    # high bias and low variance typically correspond to underfitting.

    # the degree 9 model fit the training set perfectly.
    # it has low bias, but very high variance (since any two training
    # sets would likely give rise to very different models)
    # this corresponds to overfitting.

    # if a model has high bias (performs poorly on training data even)
    # try adding more features
    # if a model has high variance, try removing features or obtaining
    # more data

    # more data will not help with bias.
    # if your model doesn't use enough features to capture regularities
    # in the data, more data won't help.
