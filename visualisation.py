#!/usr/bin/env python3
"""
Data Science from Scratch
Chapter 03: Visualising Data
"""
from collections import Counter

from matplotlib import pyplot as plt


def make_simple_line_chart():
    years = [1950, 1960, 1970, 1980, 1990, 2000, 2010]
    gdp = [300.2, 543.3, 1075.9, 2862.5, 5979.6, 10289.7, 14958.3]

    # create a line chart
    plt.plot(years, gdp, color='green', marker='o', linestyle='solid')

    # add a title
    plt.title('Nominal GDP')

    # add y-axis label
    plt.ylabel('Billions of $')
    plt.show()


def make_simple_bar_chart():
    movies = ['Annie Hall', 'Ben-Hur', 'Casablanca', 'Gandhi', 'West Side Story']
    num_oscars = [5, 11, 3, 8, 10]

    xs = [i for i, _ in enumerate(movies)]

    # plot bars with left x-coordinates [xs], heights [num_oscars]
    plt.bar(xs, num_oscars)

    plt.ylabel('# of Academy Awards')
    plt.title('My Favourite Movies')

    # label x-axis with movie names at bar centres
    plt.xticks([i for i, _ in enumerate(movies)], movies)
    plt.show()


def make_histogram():
    grades = [83, 95, 91, 87, 70, 0, 85, 82, 100, 67, 73, 77, 0]

    def decile(grade):
        return grade // 10 * 10

    histogram = Counter(decile(grade) for grade in grades)

    plt.bar(
        [x for x in histogram.keys()],
        histogram.values(),             # give each bar its correct height
        8,                              # give each bar a width of 8
    )

    # x-axis from -5 to 105, y-axis from 0 to 5
    plt.axis([-5, 105, 0, 5])

    # x-axis labels at 0, 10, ..., 1000
    plt.xticks([10 * i for i in range(11)])

    plt.xlabel('Decile')
    plt.ylabel('# of Students')
    plt.title('Distribution of Exam 1 Grades')
    plt.show()


def make_misleading_y_axis(mislead=True):
    mentions = [500, 505]
    years = [2013, 2014]

    # plt.bar([2012.6, 2013.6], mentions, 0.8)
    plt.bar([2013, 2014], mentions)
    plt.xticks(years)
    plt.ylabel('# of times I heard someone say "data science"')

    # if you don't do this, matplotlib will label the x-axis 0, 1
    # and then add a +2.013e3 off in the corner
    plt.ticklabel_format(useOffset=False)

    if mislead:
        # misleading y-axis only shows the part above 500
        plt.axis([2012, 2015, 499, 506])
        plt.title('Look at the "Huge" Increase!')
    else:
        plt.axis([2012, 2015, 0, 550])
        plt.title('Not so Huge Anymore')
    plt.show()


def make_several_line_charts():
    variance = [1, 2, 4, 8, 16, 32, 64, 128, 256]
    bias_squared = [256, 128, 64, 32, 16, 8, 4, 2, 1]
    total_error = [x + y for x, y in zip(variance, bias_squared)]
    xs = range(len(variance))

    # we can make multiple calls to plt.plot
    # to show multiple  series on the same chart
    plt.plot(xs, variance, 'g-', label='variance')  # green solid line
    plt.plot(xs, bias_squared, 'r-.', label='bias^2')  # red dot-dashed line
    plt.plot(xs, total_error, 'b:', label='total error')  # blue dotted line

    # because we've assigned labels to each series
    # we can get a legend for free
    # loc=9 means "top centre"
    # best -- 0
    # upper right -- 1
    # upper left -- 2
    # lower left -- 3
    # lower right -- 4
    # right -- 5
    # center left -- 6
    # center right -- 7
    # lower center -- 8
    # upper center -- 9
    # center -- 10
    plt.legend(loc=9)
    plt.xlabel('model complexity')
    plt.title('The Bias-Variance Tradeoff')
    plt.show()


def make_scatterplot():
    friends = [70, 65, 72, 63, 71, 64, 60, 64, 67]
    minutes = [175, 170, 205, 120, 220, 130, 105, 145, 190]
    labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']

    plt.scatter(friends, minutes)

    # label each point
    for label, friend_count, minute_count in zip(labels, friends, minutes):
        plt.annotate(
            label,
            xy=(friend_count, minute_count),  # put the label with its point
            xytext=(5, -5),  # but slightly offset
            textcoords='offset points',
        )
    plt.title('Daily Minutes vs. Number of Friends')
    plt.xlabel('# of friends')
    plt.ylabel('daily minutes spent on the site')
    plt.show()


def make_scatterplot_axes(equal_axes=False):
    test_1_grades = [99, 90, 85, 97, 80]
    test_2_grades = [100, 85, 60, 90, 70]

    plt.scatter(test_1_grades, test_2_grades)
    plt.xlabel('test 1 grade')
    plt.ylabel('test 2 grade')

    if equal_axes:
        plt.title('Axes are Comparable')
        plt.axis('equal')
    else:
        plt.title('Axes are not Comparable')

    plt.show()


if __name__ == '__main__':
    make_simple_line_chart()
    make_simple_bar_chart()
    make_histogram()
    make_misleading_y_axis(mislead=True)
    make_misleading_y_axis(mislead=False)
    make_several_line_charts()
    make_scatterplot()
    make_scatterplot_axes(equal_axes=True)
    make_scatterplot_axes(equal_axes=False)
