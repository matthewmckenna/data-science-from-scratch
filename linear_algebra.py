#!/usr/bin/env python3
"""
Data Science from Scratch
Chapter 04: Linear Algebra
"""
from functools import reduce
import math

import matplotlib.pyplot as plt

# height_weight_age = [
#     70,  # inches
#     170,  # pounds
#     40,  # years
# ]
#
# grades = [
#     95,  # exam1
#     80,  # exam2
#     75,  # exam3
#     62,  # exam4
# ]

# A = [
#     [1, 2, 3],  # A has 2 rows and 3 columns
#     [4, 5, 6],
# ]
#
# B = [
#     [1, 2],  # B has 3 rows and 2 columns
#     [3, 4],
#     [5, 6],
# ]


def vector_add(v, w):
    """adds two vectors componentwise"""
    return [v_i + w_i for v_i, w_i in zip(v, w)]


def vector_subtract(v, w):
    """subtracts two vectors componentwise"""
    return [v_i - w_i for v_i, w_i in zip(v, w)]


def vector_sum_00(vectors):
    """sums all corresponding elements"""
    result = vectors[0]
    for vector in vectors[1:]:
        result = vector_add(result, vector)
    return result


def vector_sum(vectors):
    """sums all corresponding elements"""
    return reduce(vector_add, vectors)


def scalar_multiply(c, v):
    """c is a number, v is a vector"""
    return [c * v_i for v_i in v]


def vector_mean(vectors):
    """compute the vector whose i-th element is the mean of the
    i-th elements of the input vectors"""
    n = len(vectors)
    return scalar_multiply(1/n, vector_sum(vectors))


def dot(v, w):
    """sum of the componentwise products: v_1 * w_1 + ... + v_n * w_n"""
    return sum(v_i * w_i for v_i, w_i in zip(v, w))


def sum_of_squares(v):
    """v_1 * v_1 + ... + v_n * v_n"""
    return dot(v, v)


def magnitude(v):
    """|v| is the length of `v`. |v| = sqrt(v_1 ** 2 + ... + v_n ** 2)"""
    return math.sqrt(sum_of_squares(v))


def squared_distance(v, w):
    """(v_1 - w_1) ** 2 + ... + (v_n - w_n) ** 2"""
    return sum_of_squares(vector_subtract(v, w))


def distance(v, w):
    return math.sqrt(squared_distance(v, w))


def distance_00(v, w):
    """alternative way of calculating the distance"""
    return magnitude(vector_subtract(v, w))


def shape(A):
    num_rows = len(A)
    num_cols = len(A[0]) if A else 0  # number of elements in first row
    return num_rows, num_cols


def get_row(A, i):
    return A[i]  # A[i] is the ith row


def get_column(A, j):
    return [A_i[j] for A_i in A]  # jth element of row A_i for each row in A


def make_matrix(num_rows, num_cols, entry_fn):
    """returns a num_rows x num_cols matrix
    whose (i, j)-th entry is entry_fn(i, j)"""
    return [[entry_fn(i, j)  # given i, create a list
            for j in range(num_cols)]  # [entry_fn(i, 0), ...]
            for i in range(num_rows)]  # create one list for each i


def is_diagonal(i, j):
    """1's on the 'diagonal', 0's everywhere else"""
    return 1 if i == j else 0


def make_graph_dot_product_as_vector_projection():
    v = [2, 1]
    w = [math.sqrt(.25), math.sqrt(.75)]
    c = dot(v, w)
    vonw = scalar_multiply(c, w)
    o = [0, 0]

    plt.arrow(0, 0, v[0], v[1], width=0.002, head_width=0.1, length_includes_head=True)
    plt.annotate('v', v, xytext=[v[0] + 0.1, v[1]])

    plt.arrow(0, 0, w[0], w[1], width=0.002, head_width=0.1, length_includes_head=True)
    plt.annotate('w', w, xytext=[w[0] - 0.1, w[1]])

    plt.arrow(0, 0, vonw[0], vonw[1], length_includes_head=True)
    plt.annotate('(v•w)w', vonw, xytext=[vonw[0] - 0.1, vonw[1] + 0.1])
    plt.arrow(
        v[0],
        v[1],
        vonw[0] - v[0],
        vonw[1] - v[1],
        linestyle='dotted',
        length_includes_head=True,
    )

    plt.scatter(*zip(v, w, o), marker='.')
    # plt.axis('equal')
    plt.axis([-0.5, 2.5, -0.5, 2.0])
    plt.title(
        '(v•w)w is how far the vector `v` extends in the `w` direction\n'
        'length of the vector if we project `v`  onto `w`'
    )
    plt.show()


if __name__ == '__main__':
    # make_graph_dot_product_as_vector_projection()
    # create a 5x5 identity matrix
    identity_matrix = make_matrix(5, 5, is_diagonal)
    # [
    #   [1, 0, 0, 0, 0],
    #   [0, 1, 0, 0, 0],
    #   [0, 0, 1, 0, 0],
    #   [0, 0, 0, 1, 0],
    #   [0, 0, 0, 0, 1],
    # ]

    friendships = [(0, 1), (0, 2), (1, 2), (1, 3), (2, 3), (3, 4),
                   (4, 5), (5, 6), (5, 7), (6, 8), (7, 8), (8, 9)]

    # 1 if nodes i and j are connected, 0 otherwise
    friendships = [
        [0, 1, 1, 0, 0, 0, 0, 0, 0, 0],  # user 0
        [1, 0, 1, 1, 0, 0, 0, 0, 0, 0],  # user 1
        [1, 1, 0, 1, 0, 0, 0, 0, 0, 0],  # user 2
        [0, 1, 1, 0, 1, 0, 0, 0, 0, 0],  # user 3
        [0, 0, 0, 1, 0, 1, 0, 0, 0, 0],  # user 4
        [0, 0, 0, 0, 1, 0, 1, 1, 0, 0],  # user 5
        [0, 0, 0, 0, 0, 1, 0, 0, 1, 0],  # user 6
        [0, 0, 0, 0, 0, 1, 0, 0, 1, 0],  # user 7
        [0, 0, 0, 0, 0, 0, 1, 1, 0, 1],  # user 8
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  # user 9
    ]

    # friends_of_five = [
    #     i
    #     for i, is_friend in enumerate(friendships[5])
    #     if is_friend
    # ]
