#!/usr/bin/env python3
"""
Data Science from Scratch
Chapter 06: Probability
"""
from collections import Counter
import math
import random

import matplotlib.pyplot as plt


def random_kid():
    return random.choice(['boy', 'girl'])


def gen_families():
    """generate a lot of families to verify probabilities."""
    both_girls = older_girl = either_girl = 0
    random.seed(0)

    for _ in range(100000):
        younger = random_kid()
        older = random_kid()

        if older == 'girl':
            older_girl += 1

        if older == 'girl' and younger == 'girl':
            both_girls += 1

        if older == 'girl' or younger == 'girl':
            either_girl += 1

    print(f'P(both|older): {both_girls/older_girl}')
    print(f'P(both|either): {both_girls/either_girl}')


def uniform_pdf(x):
    return 1 if x >= 0 and x < 1 else 0


def uniform_cdf(x):
    """returns the probability that a uniform random variable is less than x"""
    if x < 0:
        # uniform random is never less than 0
        return 0
    elif x < 1:
        # e.g., P(X < 0.4) = 0.4
        return x
    else:
        # uniform random is always less than 1
        return 1


def normal_pdf(x, mu=0, sigma=1):
    """normal probability density function"""
    sqrt_two_pi = math.sqrt(2 * math.pi)
    # return (math.exp(-(x-mu) ** 2 / 2 / sigma ** 2) / (sqrt_two_pi * sigma))
    return (math.exp(-(x-mu) ** 2 / (2 * sigma ** 2)) / (sqrt_two_pi * sigma))


def normal_cdf(x, mu=0, sigma=1):
    # return (1 + math.erf((x - mu) / math.sqrt(2) / sigma)) / 2
    return (1 + math.erf((x - mu) / (math.sqrt(2) * sigma))) / 2


def inverse_normal_cdf(p, mu=0, sigma=1, tolerance=0.00001):
    """find approximate inverse using binary search.

    we may want to find the value corresponding to a spcified
    probability.
    """
    # if not standard, compute standard and rescale
    if mu != 0 or sigma != 1:
        return mu + sigma * inverse_normal_cdf(p, tolerance=tolerance)

    low_z = -10.0  # normal_cdf(-10) is (very close to) 0
    hi_z = 10.0  # normal_cdf(10) is (very close to) 1

    while hi_z - low_z > tolerance:
        mid_z = (low_z + hi_z) / 2  # consider the midpoint
        mid_p = normal_cdf(mid_z)  # and the cdf's value there

        if mid_p < p:
            # midpoint is still too low, search above it
            low_z = mid_z
        elif mid_p > p:
            # midpoint is still too high, search below it
            hi_z = mid_z
        else:
            break

    return mid_z


def bernoulli_trial(p):
    return 1 if random.random() < p else 0


def binomial(p, n):
    return sum(bernoulli_trial(p) for _ in range(n))


def plot_normal_pdfs():
    """plot some normal pdfs"""
    xs = [x / 10.0 for x in range(-50, 50)]
    # plot the "standard normal distribution"
    plt.plot(xs, [normal_pdf(x, sigma=1) for x in xs], '-', label='mu=0 sigma=1')
    plt.plot(xs, [normal_pdf(x, sigma=2) for x in xs], '--', label='mu=0 sigma=2')
    plt.plot(xs, [normal_pdf(x, sigma=0.5) for x in xs], ':', label='mu=0 sigma=0.5')
    plt.plot(xs, [normal_pdf(x, mu=-1) for x in xs], '-.', label='mu=-1 sigma=1')
    plt.legend()
    plt.show()


def plot_normal_cdfs():
    xs = [x / 10.0 for x in range(-50, 50)]
    plt.plot(xs, [normal_cdf(x, sigma=1) for x in xs], '-', label='mu=0 sigma=1')
    plt.plot(xs, [normal_cdf(x, sigma=2) for x in xs], '--', label='mu=0 sigma=2')
    plt.plot(xs, [normal_cdf(x, sigma=0.5) for x in xs], ':', label='mu=0 sigma=0.5')
    plt.plot(xs, [normal_cdf(x, mu=-1) for x in xs], '-.', label='mu=-1 sigma=1')
    plt.legend(loc='lower right')
    plt.show()


def make_hist(p, n, num_points):
    data = [binomial(p, n) for _ in range(num_points)]

    # use a bar chart to show the actual binomial samples
    histogram = Counter(data)
    plt.bar(
        [x - 0.4 for x in histogram.keys()],
        [v / num_points for v in histogram.values()],
        0.8,
        color='0.75',
    )

    mu = p * n
    sigma = math.sqrt(n * p * (1 - p))

    # use a line chart to show the normal approximation
    xs = range(min(data), max(data) + 1)
    ys = [normal_cdf(i + 0.5, mu, sigma) - normal_cdf(i - 0.5, mu, sigma) for i in xs]

    plt.plot(xs, ys)
    plt.show()


if __name__ == '__main__':
    # gen_families()
    # plot_normal_pdfs()
    # plot_normal_cdfs()
    print(inverse_normal_cdf(0.1))
    # make_hist(0.75, 100, 10000)
    # print(binomial(0.5, 10))
    # print(binomial(0.5, 100))
    # print(binomial(0.5, 1000))
    # print(binomial(0.5, 10000))
