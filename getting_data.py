from collections import Counter
import csv
import json
import os
import pprint

from dateutil.parser import parse
import requests
from twython import Twython, TwythonStreamer


CONSUMER_KEY = os.environ.get('CONSUMER_KEY')
CONSUMER_SECRET = os.environ.get('CONSUMER_SECRET')
ACCESS_TOKEN = os.environ.get('ACCESS_TOKEN')
ACCESS_SECRET = os.environ.get('ACCESS_SECRET')
tweets = []


class MyStreamer(TwythonStreamer):
    """our own subclass of TwythonStreamer that specifies
    how to interact with the stream"""

    def on_success(self, data):
        """what do we do when twitter sends us data?
        here data will be a Python object representing a tweet"""
        # only want to collect English-language tweets
        if data['lang'] == 'en':
            tweets.append(data)

        # stop when we've collected enough
        if len(tweets) >= 10:
            self.disconnect()

    def on_error(self, status_code, data):
        print(status_code, data)
        self.disconnect()


def main():
    # files()
    # apis()
    # twitter_search_api()
    twitter_streaming_api()


def process(date, symbol, price):
    print(date, symbol, price)


def twitter_search_api():
    twitter = Twython(CONSUMER_KEY, CONSUMER_SECRET)

    # search for tweets containing the phrase "data science"
    for status in twitter.search(q='"data science"')['statuses']:
        user = status['user']['screen_name'].encode('utf-8')
        text = status['text'].encode('utf-8')
        print(f'{user}: {text}')
        print()


def twitter_streaming_api():
    stream = MyStreamer(
        CONSUMER_KEY,
        CONSUMER_SECRET,
        ACCESS_TOKEN,
        ACCESS_SECRET,
    )

    # starts consuming public statuses that contain the keyword "data"
    stream.statuses.filter(track='data')
    for t in tweets[:2]:
        pprint.pprint(t)


def apis():
    endpoint = 'https://api.github.com/users/joelgrus/repos'
    repos = json.loads(requests.get(endpoint).text)
    dates = [parse(repo['created_at']) for repo in repos]
    month_counts = Counter(date.month for date in dates)
    weekday_counts = Counter(date.weekday() for date in dates)
    print(month_counts)
    print(weekday_counts)

    # get the language of the last five repositories
    last_5_repositories = sorted(repos, key=lambda r: r['created_at'], reverse=True)[:5]
    # print(last_5_repositories)

    last_5_languages = [repo['language'] for repo in last_5_repositories]
    print(last_5_languages)


def files():
    print('tab delimited stock prices:')
    with open('tab_delimited_stock_prices.txt', 'r', encoding='utf-8', newline='') as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            date = row[0]
            symbol = row[1]
            closing_price = float(row[2])
            process(date, symbol, closing_price)
    print()

    print('colon delimited stock prices:')
    with open('colon_delimited_stock_prices.txt', 'r', encoding='utf-8', newline='') as f:
        reader = csv.DictReader(f, delimiter=':')
        for row in reader:
            date = row['date']
            symbol = row['symbol']
            closing_price = float(row['closing_price'])
            process(date, symbol, closing_price)
    print()

    print('writing out comma_delimited_stock_prices.txt')
    today_prices = {'AAPL': 90.91, 'MSFT': 41.68, 'FB': 64.5}

    with open('comma_delimited_stock_prices.txt', 'wt', encoding='utf-8', newline='') as f:
        writer = csv.writer(f, delimiter=',')
        for stock, price, in today_prices.items():
            writer.writerow([stock, price])
    print()


if __name__ == '__main__':
    main()
